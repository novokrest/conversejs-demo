'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var path = require('path');

var viewProfileDemoRoot = './public/view-profile-demo';
var viewProfileDemoScss = path.join(viewProfileDemoRoot, 'sass/**/*.scss');
var viewProfileDemoCss = path.join(viewProfileDemoRoot, 'css');

gulp.task('sass', function () {
  return gulp.src(viewProfileDemoScss)
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest(viewProfileDemoCss));
});
 
gulp.task('sass:watch', function () {
  gulp.watch(viewProfileDemoScss, ['sass']);
});