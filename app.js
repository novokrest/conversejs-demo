const config = require('config');
const jsonfile = require('jsonfile');
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

app.use(express.static(__dirname + '/public'));

app.get('/profiles', (req, res) => {
    var profiles = jsonfile.readFileSync('resources/profiles.json');
    res.json(profiles);
});

const port = config.server.port;
app.listen(port, function () {
  console.log('Example app listening on port ' + port + '...');
});