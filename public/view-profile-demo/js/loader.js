(function (root, factory) {
    define('loader', ['converse'], factory);
})(this, function (converse) {

    var loader = {

        initialize: function (username, password) {
            console.log('loader initializing...');

            converse.initialize({
                whitelisted_plugins: ['demo-plugin'],
                bosh_service_url: 'https://conversejs.org/http-bind/',
                authentication: 'login',
                allow_logout: true,
                allow_contact_removal: true,
                show_controlbox_by_default: false,
                show_desktop_notifications: false,
                keepalive: false,
                play_sound: false
            });
        }
    };

    return loader;
});