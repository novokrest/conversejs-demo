(function (root, factory) {
    define('login-form', ['jquery', 'converse'], factory);
})(this, function ($, converse) {

    var _ = converse.env._;

    var loginForm = {

        initialize(_converse) {
            console.log('login-form: initialize()');
            this._converse = _converse;
            this._logIn = this._logIn.bind(this);
            this._logOut = this._logOut.bind(this);
            this._toLogInMode();
        },

        get user() {
            return this._userInput.val();
        },

        get password() {
            return this._passwordInput.val();
        },

        _toLogInMode() {
            this._isLoggedIn = false;
            this._loginButtonCommand = this._logIn;
            this._loginButton.val('Log In');
            this._enableUserInput();
            this._enablePasswordInput();
            this._hideMessanger();
        },

        _logIn() {
            var username = this.user;
            var password = this.password;
            this._toConnectingMode();
            this._converse.api.listen.once('initialized', this._onConnected.bind(this));
            this._converse.connection.connect(username, password, this._converse.onConnectStatusChanged);
        },

        _onConnected() {
            console.log('login-form: onConnected()');
            this._toLogOutMode();
        },

        _enableUserInput() {
            this._userInput.prop('disabled', false);
        },

        _enablePasswordInput() {
            this._passwordInput.prop('disabled', false);
        },

        _hideMessanger() {
            console.log('conversejs:', this._messanger);
            this._messanger.hide();
        },

        _toConnectingMode() {
            this._loginButtonCommand = _.noop;
            this._loginButton.val('Connecting...');
            this._disableUserInput();
            this._disablePasswordInput();
            this._hideMessanger();
        },

        _toLogOutMode() {
            this._isLoggedIn = true;
            this._loginButtonCommand = this._logOut;
            this._loginButton.val('Log Out');
            this._disableUserInput();
            this._disablePasswordInput();
            this._showMessanger();
        },

        _logOut() {
            this._converse.api.user.logout();
            this._toLogInMode();
        },

        _disableUserInput() {
            this._userInput.prop('disabled', true);
        },

        _disablePasswordInput() {
            this._passwordInput.prop('disabled', true);
        },

        _showMessanger() {
            this._messanger.show();
        },

        get _userInput() {
            return $('.user-input');
        },

        get _passwordInput() {
            return $('.password-input');
        },

        set _loginButtonCommand(callback) {
            this._loginButton.off();
            this._loginButton.click(callback);
        },

        get _loginButton() {
            return $('.login-btn');
        },

        get _messanger() {
            return $('#conversejs');
        }
    };

    return loginForm;
});