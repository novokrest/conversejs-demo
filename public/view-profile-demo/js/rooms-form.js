(function (root, factory) {
    define('rooms-form', ['jquery', 'converse'], factory);
})(this, function ($, converse) {
    var _ = converse.env._,
        $iq = converse.env.$iq,
        Strophe = converse.env.Strophe;

    this.roomsForm = {
        
        initialize(_converse) {
            this._converse = _converse;
            this._updateRooms = this._updateRooms.bind(this);
            this._fetchRooms = this._fetchRooms.bind(this);
            this._fetchMucButton.click(this._fetchRooms);
            
            _converse.api.listen.on('initialized', this.onInitialized.bind(this));
            _converse.api.listen.on('logout', this.onLogout.bind(this));
            _converse.api.listen.on('rosterGroupsFetched', this._updateRooms);

            this._hide();
        },

        onInitialized() {
            this._show();
        },

        onLogout() {
            this._hide();
        },

        _updateRooms() {
            var _converse = this._converse;
            var rooms = _converse.api.rooms.get();
            console.log('rooms-form', 'rooms', rooms);
        },

        _fetchRooms() {
            var _converse = this._converse;
            console.log('_fetchRooms', this.mucDomain);
            /* Send and IQ stanza to the server asking for all rooms */
            _converse.connection.sendIQ(
                $iq({
                    to: this.mucDomain,
                    from: _converse.connection.jid,
                    type: "get"
                }).c("query", {xmlns: Strophe.NS.DISCO_ITEMS}),
                this.onRoomsFetched.bind(this),
                this.onFetchRoomsFailed.bind(this)
            );
        },

        onRoomsFetched(iq) {
            var rooms = $(iq).find('query').find('item');
            console.log('onRoomsFetched', rooms);
            if (rooms.length == 0) {
                this.onFetchRoomsFailed();
                return;
            }

            var roomsList = this._roomsList;
            roomsList.empty();
            roomsList.append($(
                '<li class="list-group-item list-group-item-info">' + 
                    '<div class="row room-row-header">' +
                        '<div class="col-xs-6 col-md-6 col-lg-6">' +
                            'JID' +
                        '</div>' + 
                        '<div class="col-xs-4 col-md-4 col-lg-4">' +
                            'Name' +
                        '</div>' + 
                        '<div class="col-xs-2 col-md-2 col-lg-2"></div>' + 
                    '</div>' +
                '</li>'
            ));

            _.each(rooms, function (room) {
                var $room = $(room);
                
                const jid = $room.attr('jid');
                const name = Strophe.unescapeNode(
                    $room.attr('name') || $room.attr('jid')
                );
                
                roomsList.append($(
                    '<li class="list-group-item">' + 
                        '<div class="row room-row">' +
                            '<div class="col-xs-6 col-md-6 col-lg-6">' +
                                '<span class="room-jid-span">' +
                                    jid +
                                '</span>' +
                            '</div>' + 
                            '<div class="col-xs-4 col-md-4 col-lg-4">' +
                                name +
                            '</div>' + 
                            '<div class="col-xs-1 col-md-1 col-lg-1">' +
                                '<span class="btn btn-xs btn-info info-room-btn">' +
                                    'Info' +
                                '</span>' +
                            '</div>' + 
                            '<div class="col-xs-1 col-md-1 col-lg-1">' +
                                '<span class="btn btn-xs btn-danger join-room-btn">' +
                                    'Join' +
                                '</span>' +
                            '</div>' + 
                        '</div>' +
                    '</li>'
                ));
            });

            $('.info-room-btn').click(this._viewRoomInfo.bind(this));
            $('.join-room-btn').click(this._joinRoom.bind(this));
        },

        _viewRoomInfo(e) {
            var _converse = this._converse;
            var roomJid = $(e.target).parents('.room-row').find('.room-jid-span').text();
            console.log('rooms-form', '_viewRoomInfo()', 'roomJid=', roomJid);
            var that = this;
            _converse.connection.disco.info(roomJid, null, function (stanza) {
                var $stanza = $(stanza);
                var roomInfo = {
                    'jid': stanza.getAttribute('from'),
                    'desc': $stanza.find('field[var="muc#roominfo_description"] value').text(),
                    'occ': $stanza.find('field[var="muc#roominfo_occupants"] value').text(),
                    'hidden': $stanza.find('feature[var="muc_hidden"]').length,
                    'membersonly': $stanza.find('feature[var="muc_membersonly"]').length,
                    'moderated': $stanza.find('feature[var="muc_moderated"]').length,
                    'nonanonymous': $stanza.find('feature[var="muc_nonanonymous"]').length,
                    'open': $stanza.find('feature[var="muc_open"]').length,
                    'passwordprotected': $stanza.find('feature[var="muc_passwordprotected"]').length,
                    'persistent': $stanza.find('feature[var="muc_persistent"]').length,
                    'publicroom': $stanza.find('feature[var="muc_public"]').length,
                    'semianonymous': $stanza.find('feature[var="muc_semianonymous"]').length,
                    'temporary': $stanza.find('feature[var="muc_temporary"]').length,
                    'unmoderated': $stanza.find('feature[var="muc_unmoderated"]').length,
                }
                console.log(roomInfo);
                alert(that._buildRoomInfoString(roomInfo));
            });
        },

        _buildRoomInfoString(roomInfo) {
            var jid = roomInfo.jid;
            var server = roomInfo.jid.match(/^(.*)@(.*)$/)[2];
            return [
                'JID: ', jid, '\n',
                'Server: ', server, '\n',
                'Occupants: ', roomInfo.occ, '\n',
                'Hidden: ', !!roomInfo.hidden, '\n',
                'Members only: ', !!roomInfo.membersonly, '\n',
                'Moderated: ', !!roomInfo.moderated, '\n',
                'Anonymous: ', !!roomInfo.nonanonymous, '\n',
                'Open: ', !!roomInfo.open, '\n',
                'Secured: ', !!roomInfo.passwordprotected, '\n',
                'Persistent: ', !!roomInfo.persistent, '\n',
                'Public: ', !!roomInfo.publicroom, '\n',
                'Temporary: ', !!roomInfo.temporary, '\n',
            ].join('');
        },

        _joinRoom(e) {
            var _converse = this._converse;
            var roomJid = $(e.target).parents('.room-row').find('.room-jid-span').text();
            _converse.api.rooms.open(roomJid);
        },

        onFetchRoomsFailed() {
            var roomsList = this._roomsList;
            roomsList.empty();
            roomsList.append($(
                '<li class="list-group-item">No groups were found</li>'
            ));
        },

        _show() {
            this._container.show();
        },

        _hide() {
            this._container.hide();
        },

        get _container() {
            return $('.muc-search-container');
        },

        get mucDomain() {
            return this._mucDomain.val();
        },

        get _mucDomain() {
            return $('.muc-domain-input');
        },

        get _fetchMucButton() {
            return $('.fetch-muc-btn');
        },

        get _roomsList() {
            return $('.found-rooms-list');
        }
    };

    return roomsForm;
});