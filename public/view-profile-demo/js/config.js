require.config({
    baseUrl: '.',
    paths: {
        'jquery': 'https://code.jquery.com/jquery-3.2.1.min',
        'converse': 'https://cdn.conversejs.org/dist/converse.min',
        'buddies-form': 'js/buddies-form',
        'demo': 'js/demo',
        'demo-plugin': 'js/demo-plugin',
        'loader': 'js/loader',
        'login-form': 'js/login-form',
        'mam-form': 'js/mam-form',
        'own-room-form': 'js/own-room-form',
        'profile-form': 'js/profile-form',
        'rooms-form': 'js/rooms-form',
        'user-form': 'js/user-form',
        'vcard-form': 'js/vcard-form'
    }
});