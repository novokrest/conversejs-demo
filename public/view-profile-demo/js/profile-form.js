(function (root, factory) {
    define('profile-form', ['jquery', 'converse'], factory);
})(this, function ($, converse) {

    // TODO: use key-mirror library
    var ProfileType = {
        Friend: 'Friend',
        User: 'User',
        Me: 'Me',
    };

    this.profileForm = {

        initialize(_converse) {
            this._converse = _converse;

            this._viewProfile = this._viewProfile.bind(this);
            this._viewProfileButton.click(this._viewProfile);

            this._addBuddy = this._addBuddy.bind(this);
            this._addBuddyButton.click(this._addBuddy);

            this._sendMessage = this._sendMessage.bind(this);
            this._sendMessageButton.click(this._sendMessage);
            
            this._updateProfile = this._updateProfile.bind(this);
            _converse.api.listen.on('rosterContactsFetched', this._updateProfile);
            _converse.api.listen.on('contactStatusChanged', this._updateProfile);

            _converse.api.listen.on('initialized', this.onInitialize.bind(this));

            this._hide = this._hide.bind(this);
            this._disableSearch = this._disableSearch.bind(this);
            _converse.api.listen.on('logout', this._hide);
            _converse.api.listen.on('logout', this._disableSearch);
        },

        onInitialize() {
            this.searchedJid = this.currentJid;
            this._enableSearch();
        },

        _viewProfile() {
            this._updateProfile();
            this._show();
        },

        get _profileInfo() {
            var _converse = this._converse;
            
            var profileJid = this.searchedJid;
            var profileInfo = {
                jid: profileJid
            };

            // _converse.api.user.jid() returns full jid as 'jid/resource'
            var currentJid = this.currentJid;
            console.log('currentJid:', currentJid, 'profileJid:', profileJid);
            if (currentJid === profileJid) {
                profileInfo.status = 'me';
                profileInfo.type = ProfileType.Me;
                return profileInfo;
            }

            var contacts = _converse.api.contacts.get();
            var contactByJid = _.reduce(contacts, function (result, contact) {
                result[contact.jid] = contact;
                return result;
            }, {});
            var isFriend = _.has(contactByJid, profileJid);
            
            if (isFriend) {
                profileInfo.status = contactByJid[profileJid].chat_status;
                profileInfo.type = ProfileType.Friend;
                return profileInfo; 
            }

            profileInfo.type = ProfileType.User;
            profileInfo.status = 'unknown';
            return profileInfo;
        },

        _updateProfile() {
            var profileInfo = this._profileInfo;
            console.log('profile-form', 'profileInfo', profileInfo);

            switch (profileInfo.type) {
                case ProfileType.Me:
                    this._addBuddyButton.hide();
                    this._sendMessageButton.hide();
                    break;

                case ProfileType.Friend:
                    this._addBuddyButton.hide();
                    this._sendMessageButton.show();
                    break;

                case ProfileType.User:
                    this._addBuddyButton.show();
                    this._sendMessageButton.hide();
                    break;
            }

            this.status = profileInfo.status;
            this.jid = profileInfo.jid;
        },

        _show() {
            this._container.show();
        },

        _hide() {
            this._container.hide();
        },

        _addBuddy() {
            var _converse = this._converse;
            _converse.api.contacts.add(this.jid);
        },

        _sendMessage() {
            var _converse = this._converse;
            var chat = _converse.api.chats.open(profileForm.jid);
            chat.show();

            // var msg = converse.env.$msg({
            //     from: loginForm.user,
            //     to: profileForm.jid,
            //     type:'chat'
            // }).c('body').t('lol').up()
            //   .c('active', {'xmlns': 'http://jabber.org/protocol/chatstates'}).tree();
            // _converse.api.send(msg);
        },

        get jid() {
            return this._jid.val();
        },

        set jid(jid) {
            this._jid.val(jid);
        },

        get status() {
            return this._status.text();
        },

        set status(status) {
            this._status.text(status);
        },

        get searchedJid() {
            return this._searchedJid.val();
        },

        set searchedJid(jid) {
            this._searchedJid.val(jid);
        },

        get currentJid() {
            return this._converse.api.user.jid().match(/^(.*)\/(.*)$/)[1];
        },

        _enableSearch() {
            this._searchContainer.show();
        },

        _disableSearch() {
            this._searchContainer.hide();
        },

        get _jid() {
            return $('.profile-jid');
        },

        get _searchedJid() {
            return $('.profile-jid-input');
        },

        get _status() {
            return $('.profile-status-span');
        },

        get _addBuddyButton() {
            return $('.add-buddy-btn');
        },

        get _sendMessageButton() {
            return $('.send-message-btn');
        },

        get _viewProfileButton() {
            return $('.view-profile-btn');
        },

        get _container() {
            return $('.profile-view-container')
        },

        get _searchContainer() {
            return $('.profile-search-container');
        }
    };

    return this.profileForm;
});