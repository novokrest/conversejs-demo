(function (root, factory) {
    define('user-form', ['jquery', 'converse'], factory);
})(this, function ($, converse) {

    this.userForm = {

        initialize(_converse) {
            this._converse = _converse;
            this._changeStatus = this._changeStatus.bind(this);
            this._statusMenuElements.click(this._changeStatus);
            this.hide();

            _converse.api.listen.on('initialized', this.onInitialized.bind(this));
            _converse.api.listen.on('statusChanged', this.onStatusChanged.bind(this));
            _converse.api.listen.on('logout', this.hide.bind(this));
        },

        onInitialized() {
            console.log('user-form', 'onInitialized()');
            var _converse = this._converse;
            var status = _converse.api.user.status.get();
            this.status = status;
            this.show();
        },

        show() {
            this._container.show();
        },

        hide() {
            this._container.hide();
        },

        _changeStatus(event) {
            var _converse = this._converse;
            var status = $(event.target).text();
            _converse.api.user.status.set(status);
        },

        onStatusChanged() {
            this.status = this._converse.api.user.status.get();
        },

        get status() {
            return this._status.text();
        },

        set status(status) {
            this._status.text(status);
        },

        get _status() {
            return $('.status-badge');
        },

        get _container() {
            return $('.user-container');
        },

        get _statusMenuElements() {
            return $('.status-menu li');
        }
    };

    return this.userForm;
});