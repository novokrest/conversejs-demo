(function (root, factory) {
    define('vcard-form', ['jquery', 'converse'], factory);
})(this, function ($, converse) {

    var vcardForm = {

        initialize(_converse) {
            this._converse = _converse;
            _converse.api.listen.on('initialized', this.onInitialized.bind(this));
            this.hide();
        },

        onInitialized() {
            console.log('vcard-form', 'onInitialized');
            this._converse.getVCard(
                null, // No 'to' attr when getting one's own vCard
                this.onVcardFetched.bind(this),
                this.onVcardFailed.bind(this)
            );
        },

        onVcardFetched(iq, jid, fullname, img, imgType, url) {
            var _converse = this._converse;

            this.fullname = fullname;
            this.image = img;
            this.imageType = imgType;
            this.url = url;

            this.show();
        },

        onVcardFailed(iq, jid) {
            console.log('Failed to fetch vcard info', iq, jid);
            var _converse = this._converse;

            var errorMessage = 'VCard is not available';
            this.fullname = errorMessage;
            this.image = errorMessage;
            this.imageType = errorMessage;
            this.url = errorMessage;

            this.show();
        },

        show() {
            this._container.show();
        },

        hide() {
            this._container.hide();
        },

        get _container() {
            return $('.vcard-container');
        },

        get fullname() {
            return this._fullname.val();
        },

        set fullname(fullname) {
            this._fullname.val(fullname);
        },

        get _fullname() {
            return $('.vcard-fullname-info');
        },

        get image() {
            return this._image.val();
        },

        set image(image) {
            this._image.val(image);
        },

        get _image() {
            return $('.vcard-image-info');
        },

        get imageType() {
            return this._imageType.val();
        },

        set imageType(imageType) {
            this._imageType.val(imageType);
        },

        get _imageType() {
            return $('.vcard-image-type-info');
        },

        get url() {
            return this._url.val();
        },

        set url(url) {
            this._url.val(url);
        },

        get _url() {
            return $('.vcard-url-info');
        },
    };

    return vcardForm;
});