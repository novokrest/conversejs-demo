(function (root, factory) {
    define('buddies-form', ['jquery', 'converse'], factory);
})(this, function ($, converse) {

    var _ = converse.env._
        $iq = converse.env.$iq,
        Strophe = converse.env.Strophe;

    var buddiesForm = {

        initialize(_converse) {
            this._converse = _converse;
            this._updateBuddies = this._updateBuddies.bind(this);
            _converse.api.listen.on('initialized', this._updateBuddies);
            _converse.api.listen.on('rosterContactsFetched', this._updateBuddies);
            _converse.api.listen.on('contactStatusChanged', this._updateBuddies);
            _converse.api.listen.on('contactRemoved', this._updateBuddies);
        },

        _updateBuddies(removedBuddyJid) {
            console.log('buddies-form', '_updateBuddies()', removedBuddyJid);
            var buddiesList = this._buddiesList;
            buddiesList.empty();

            var contacts = _.filter(this.contacts, function (contact) {
                return contact.jid !== removedBuddyJid;
            });
            if (contacts.length == 0) {
                buddiesList.append($('<li class="list-group-item">No buddies</li>'))
                return;
            }

            // TODO: implement through templates
            var contactItems = _.map(contacts, function(contact) {
                return $(
                    '<li class="list-group-item">' + 
                        '<div class="row buddy-row">' +  
                            '<div class="col-xs-8">' + 
                                '<span class="buddy-jid pull-left">' + 
                                    contact.jid +
                                '</span>' +
                            '</div>' + 
                            '<div class="col-xs-2">' + 
                                '<span class="pull-center">' + 
                                    '<span class="badge">' + 
                                        contact.chat_status +
                                    '</span>' +
                                '</span>' +
                            '</div>' + 
                            '<div class="col-xs-2">' + 
                                '<span class="pull-right">' + 
                                    '<span class="btn btn-xs btn-danger remove-buddy-btn">' +
                                        'Remove' +
                                    '</span>' +
                                '</span>' + 
                            '</div>' + 
                        '</div>' + 
                    '</li>'
                );
            });

            _.forEach(contactItems, function(contactItem) {
                buddiesList.append(contactItem);
            });

            $('.remove-buddy-btn').click(this._removeBuddy.bind(this));
        },

        _removeBuddy(e) {
            var _converse = this._converse;
            var buddyJidToRemove = $(e.target).parents('.buddy-row').find('.buddy-jid').text();

            if (!buddyJidToRemove || !_converse.allow_contact_removal) { return; }
            var result = confirm("Are you sure you want to remove this contact?");
            if (result === true) {
                var iq = $iq({type: 'set'})
                    .c('query', {xmlns: Strophe.NS.ROSTER})
                    .c('item', {jid: buddyJidToRemove, subscription: "remove"});
                _converse.connection.sendIQ(iq,
                    function() {
                        this._updateBuddies(buddyJidToRemove);
                    }.bind(this),
                    function (err) {
                        alert("Sorry, there was an error while trying to remove " + buddyJidToRemove + " as a contact.");
                        _converse.log(err, Strophe.LogLevel.ERROR);
                    }
                );
            }
        },

        get contacts() {
            return this._converse.api.contacts.get();
        },

        get _buddiesList() {
            return $('.buddy-list');
        }
    };

    return buddiesForm;
});