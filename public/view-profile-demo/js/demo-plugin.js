(function (root, factory) {
    // AMD. Register as a module called "demo-plugin"
    define(
        'demo-plugin', 
        [
            'jquery',
            'converse', 
            'login-form',
            'own-room-form',
            'profile-form', 
            'user-form', 
            'buddies-form',
            'rooms-form',
            'mam-form',
            'vcard-form'
        ], 
        factory
    );
})(this, function (
    $,
    converse, 
    loginForm,
    ownRoomForm,
    profileForm, 
    userForm, 
    buddiesForm, 
    roomsForm, 
    mamForm,
    vcardForm
) {
    // Commonly used utilities and variables can be found under the "env"
    // namespace of the "converse" global.
    var Strophe = converse.env.Strophe,
        $iq = converse.env.$iq,
        $msg = converse.env.$msg,
        $pres = converse.env.$pres,
        $build = converse.env.$build,
        b64_sha1 = converse.env.b64_sha1,
        _ = converse.env._,
        moment = converse.env.moment;

    var isProfileInitialized = false,
        contacts = {};

    converse.plugins.add('demo-plugin', {

        overrides: {
            RosterContact: {
                initialize (attributes) {
                    this.__super__.initialize.apply(this, arguments);
                    this.set({
                        'image_type': 'image/png',
                        // 'image': 'iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAIAAABt+uBvAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwHCy455JBsggAABkJJREFUeNrtnM1PE1sUwHvvTD8otWLHST/Gimi1CEgr6M6FEWuIBo2pujDVsNDEP8GN/4MbN7oxrlipG2OCgZgYlxAbkRYw1KqkIDRCSkM7nXvvW8x7vjyNeQ9m7p1p3z1LQk/v/Dhz7vkEXL161cHl9wI5Ag6IA+KAOCAOiAPigDggLhwQB2S+iNZ+PcYY/SWEEP2HAAAIoSAIoihCCP+ngDDGtVotGAz29/cfOXJEUZSOjg6n06lp2sbGRqlUWlhYyGazS0tLbrdbEASrzgksyeYJId3d3el0uqenRxRFAAAA4KdfIIRgjD9+/Pj8+fOpqSndslofEIQwHA6Pjo4mEon//qmFhYXHjx8vLi4ihBgDEnp7e9l8E0Jo165dQ0NDd+/eDYVC2/qsJElDQ0OEkKWlpa2tLZamxAhQo9EIBoOjo6MXL17csZLe3l5FUT59+lQul5l5JRaAVFWNRqN37tw5ceKEQVWRSOTw4cOFQuHbt2+iKLYCIISQLMu3b99OJpOmKAwEAgcPHszn8+vr6wzsiG6UQQhxuVyXLl0aGBgwUW0sFstkMl6v90fo1KyAMMYDAwPnzp0zXfPg4GAqlWo0Gk0MiBAiy/L58+edTqf5Aa4onj59OhaLYYybFRCEMBaL0fNxBw4cSCQStN0QRUBut3t4eJjq6U+dOiVJElVPRBFQIBDo6+ujCqirqyscDlONGykC2lYyYSR6pBoQQapHZwAoHo/TuARYAOrs7GQASFEUqn6aIiBJkhgA6ujooFpUo6iaTa7koFwnaoWadLNe81tbWwzoaJrWrICWl5cZAFpbW6OabVAEtLi4yABQsVjUNK0pAWWzWQaAcrlcswKanZ1VVZUqHYRQEwOq1Wpv3ryhCmh6erpcLjdrNl+v1ycnJ+l5UELI27dvv3//3qxxEADgy5cvExMT9Mznw4cPtFtAdAPFarU6Pj5eKpVM17yxsfHy5cvV1VXazXu62gVBKBQKT58+rdVqJqrFGL948eLdu3dU8/g/H4FBUaJYLAqC0NPTY9brMD4+PjY25mDSracOCABACJmZmXE6nUePHjWu8NWrV48ePSKEsGlAs7Agfd5nenq6Wq0mk0kjDzY2NvbkyRMIIbP2PLvhBUEQ8vl8NpuNx+M+n29bzhVjvLKycv/+/YmJCcazQuwA6YzW1tYmJyf1SY+2trZ/rRk1Go1SqfT69esHDx4UCgVmNaa/zZ/9ABUhRFXVYDB48uTJeDweiUQkSfL7/T9MA2NcqVTK5fLy8vL8/PzU1FSxWHS5XJaM4wGr9sUwxqqqer3eUCgkSZJuUBBCfTRvc3OzXC6vrKxUKhWn02nhCJ5lM4oQQo/HgxD6+vXr58+fHf8sDOp+HQDg8XgclorFU676dKLlo6yWRdItIBwQB8QBcUCtfosRQjRNQwhhjPUC4w46WXryBSHU1zgEQWBz99EFhDGu1+t+v//48ePxeFxRlD179ng8nh0Efgiher2+vr6ur3HMzMysrq7uTJVdACGEurq6Ll++nEgkPB7Pj9jPoDHqOxyqqubz+WfPnuVyuV9XPeyeagAAAoHArVu3BgcHab8CuVzu4cOHpVKJUnfA5GweY+xyuc6cOXPv3r1IJMLAR8iyPDw8XK/Xi8Wiqqqmm5KZgBBC7e3tN27cuHbtGuPVpf7+/lAoNDs7W61WzfVKpgHSSzw3b95MpVKW3MfRaDQSiczNzVUqFRMZmQOIEOL1eq9fv3727FlL1t50URRFluX5+flqtWpWEGAOIFEUU6nUlStXLKSjy759+xwOx9zcnKZpphzGHMzhcDiTydgk9r1w4YIp7RPTAAmCkMlk2FeLf/tIEKbTab/fbwtAhJBoNGrutpNx6e7uPnTokC1eMU3T0um0DZPMkZER6wERQnw+n/FFSxpy7Nix3bt3WwwIIcRgIWnHkkwmjecfRgGx7DtuV/r6+iwGhDHev3+/bQF1dnYaH6E2CkiWZdsC2rt3r8WAHA5HW1ubbQGZcjajgOwTH/4qNko1Wlg4IA6IA+KAOKBWBUQIsfNojyliKIoRRfH9+/dut9umf3wzpoUNNQ4BAJubmwz+ic+OxefzWWlBhJD29nbug7iT5sIBcUAcEAfEAXFAHBAHxOVn+QMrmWpuPZx12gAAAABJRU5ErkJggg==',
                        'image': 'iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAYAAABccqhmAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAYdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuNvyMY98AABBeSURBVHhe7Z0tlDTFFYYjIhARiIiIiIgIBCICgUAgIhARCAQCgYiIiEAgEJgcRAQiAoFAREREICIQCAQCgUBEREREREQgIiIiIiJIPQOzmd15Z6a7p3+qup73nGfO7vvtzrfTXd1ddevWre998803ItIp0RSRPoimiPRBNEWkD6IpIn0QTRHpg2iKSB9EU0T6IJoi0gfRFJE+iKaI9EE0RaQPoikifRBNEemDaIpIH0RTRPogmiLSB9EUkT6Ipoj0QTRFpA+iKSJ9EE0R6YNoikgfRFNE+iCaItIH0RSRPoimiPRBNEWkD6IpIn0QTRHpg2iKSB9EU0T6IJoi0gfRFJE+iKaI9EE0RaQPoikifRBNEemDaIpIH0RTRPogmiLSB9EUkT6Ipoj0QTTljB8XflJ4sfDyd/yi8OYFjj/zUoHfA94jvbfIZkSzM75f+FmBC/rXhd8W/lD4ovD3wtziPT8v8H+8V/hVgf/7+cKzhfQ3iixCNHfMDwo8ld8qfFT4c+G/hZr01wI3B/7GFwrPFNJnEbmbaO4Iut10yf9Y+FuhRf2n8FXhg8IbhR8V0mcVGU00G4an5SuF9wt/KexV3BAYPhCTSMdBZBDRbAzGzb8sfFbgadmb/llgyPB6wRiCjCKaDUDgjsDZx4UeL/pLIp7xpwLHhmOUjp3IA9GsGIJivyvw1FPX9XWBGY3nCulYijRxA+BJRveWiL2api8LDJOYBUnHWDolmpVAY2UqbIm5+F71r8JvCsYK5EA0N4ZpLrquNFa1jLwRyIFobgSNkfF9bYk5e5Y3gs6J5sowd/92wSf+duLYv1sw67AzorkiBPdazdDbozgXJFKlcyU7JJorwOIbItOqTpFLwArGdO5kR0RzQZjSY8zpOL9+/bvwTsGEoh0TzYUgIcWnfntidSI9tnROpXGiuQCsszdlt11x7qhbkM6tNEw0Z4Q5fYpfqH2IZdVmE+6IaM4E3cZ/FNS+xEyBQ4KdEM0ZYHqPIJLapxgSsLYgnXtpiGjeCVF+1YdI2U5tQBohmhNhbMj8sepH9AQI8Kb2IA0QzQlw8VNFV+1fxHUoqErREVOHGyeaI/Hi37+oQcjQjoIsqQ1Io0RzBF78+xVPegqP/rSQzr3sgGgOxIt/f2Lm5veFnxfSOZedEc0BePHvS5Rbs2RYh0RzAJShVu3r04LLfzsmmjegcIRqV0zd0c03m09G3wB4WriUt01x3j4suLWYPBDNC7Cc1/TeNkWCVgvRfIqQsHkrqeTs6UipOKYfgaxDei4Jakkef44e6uk27Xxu8xUuEM0ARSNZF67aEisxa9o/kCAjf89xG3ZWFxJMXqP0OxulkM/AzZAbBjcXZjt+WEh/axdEM8DOtKodcUG9Wkjnci24sBgycqGxhVvNm7WS88CNgbwHjls35dCi+QTu2KoNMc7nybpFl5en+2sF4gx72MyFSsn0UJgeZZv59JmbJ5onUA/Orn8boiu95j6AtA0eDoy7+b/3HhzmOmDoQK9mN/kS0TyBk6vqFk+qNdfmE6RjMVDPG7QylcqQgR5P0wHGaH4HTxOn/OoWT941uqdE0nkYuE/jubgBM+zhxpiOXdVE8ztc21+vuDFzQS5ZspuZHwqBmvI9XJRL47w0E0SMZoEsMVWniFgvuViHRKH3C27VNl3coAkgVp9tGc2CT/86Re7+UvPWNFaSahz2zStyMSieko755iTTp3+dYnpviS4/vQluLGpZkQdBdmJVOy0lk6eAqkc8kWk46VzdAzf6zwpqXREnYPYgnZPVeWoQ7bULWI9IX507uswY367+9iK4unma9lODVEhVh+gyzhlNZr6aCLULuuoSwcLNFmo9Ndyrvw7xdJgz24wgFL0JVadILFp6Wjdy+o3Bvzo058XP+5C1p9oQpdmeL6RzuQin39j9315zXvyshTdzrz3RG2AF5Sq9gdNv7P5vq7kufsb6JPIY5Gtb5A8snlF4/MLu/7ZipdkcFz/BpJrX3atxImC76JTh8Yu3CmobEZybY0EPCT2m7+5TBAjTOb+b4xeW+d5G3OHn2G6LElt2+fctqirNFR964PiF3cb1xQV7b444gSKj/P2IWYJZl3/zwl1Fra97t9Umo49AkepLs2aH8sLYUa0rntpnJ2MEXPyWautXDB1nuQnwwpyjWk9Mt94zlvPiV2iWmwAvzBmrdcS4/56gnxe/OtXdNwFeXP67nuhtnZ2EgXjxq6S7bgK8fFJQy4u192cnYCAMGYgAK5U0eTqZFyPJy4v87nvSOlkyqtQ1USuSXmJqPxfhxW7l8mLDyrODPxC3Y1dDxXqSUYuIeHHF2LIi6j918wgShZQao1FTzLx4A1hWUzfpdDt2NVWDk8x48QawnKi2e3bQB0DQz+XZaqqYbh5Ub5AX9kxXy2hqdRdzM9S9IrZ3c+jJi7MAy4jNVc4O+AC4c7uyT82hm8uIeTEPYBlRZOXsgN+ACK7z/Wou8SC5umU8L+x5rubV1Kf/OwWl5tSXhYtTg7ywA6yaV1Oe/typSRhSam5xjac2d7gBuBx4XjGkOjvQA3BNhlpK1BCIAUFeSB9U8+mNwtmBvoFbsqmlRd3Ps7Z3/MJikvOIxJ0pWX8+/dXSir2A4xcErdT9orjqowM8AGq8+fRXa+gsQ/D4hWXB5xHxlEcHeADOwqi1RHbpoxmB4xesJVb3ieWYo1ZiFX5YMPKv1tSjGNXxCxquC0/u04eFhwM7kF8WlFpTjwrTnDZGA1H3acoWTpwMpdYU8SZ6noc2eNoYzQe4T88WTo/nLQz+qa1Ez/PQDp82SsaxarzI3396LG9h4FVtpYdhwNNG+duCGi+O29NjeQvKNym1hR6GAU8bpRlp0/RK4emxvAYHX6ktdRgGpMZpUtB4PQRVBmKtP7W1DrUDU+OkIIUaLtKo03G8BoUalNpSh7hVapzAGmI1TByrdAyvYREWtbUY6j+TGicwplXDNCUB6J8FpbbWi6lxHqGirbqtsfv9Mf+vVA16KzXQI84IDBMBvXT8LkHGoFI16KPUQE+xPPVtvVxIx+4SbxaUqkGfpAZ6ihtU3NbYTT8t/Klq0eepgT6FApcuWb2ssTcApwBVLfpbaqAJKwdfFr2kdMwu4apLVYv+nhroJdyjPisdq2tQNkypKpQa6CVY7upGoudKx+oabsWmqlFqoNegdJhTg4+VjtM1zAJU1Sg10Fu4jv2xxhYCMQagqlFqoEMwHvB/jZ0FsAqwqkWjgoCnUET044L6dvVkOkaXcBpQ1aLJNwDwJvCtxmYCOqWqatFXqYGOwZvA+L0ATQVWteiPqYGOpfebwEOF1YFYcEXVondTA50CNwHWxfcoxvTpmFyCDRqdSlU16PXUQO+hxwg3vZ90LK5BOSalttbPUuO8l95Ki7NaMh2Ha1CQUaktxQK/iyXB7uXVAsUye9HYqsDuCai2FinpF4uCzgEJMl8VetDYqkAssVZqSx1iV6lxzgnBwR7iAu8W0ue/BMfFwqBqSx02s0mNcwn2PiRggU/63NcwDqC2EuP/Qx2L1DCXYs9DAp7m6TNfw92Y1VZiLc+hHT5tlEtzHBLscR6cKsrpM1+CY+FuzGoLPcSsnjbKtXi+sLd9B8bGAcCVgWpt0Vvl4XNog08b5NqwkGYvSTGHvdZG8lJBqTXFQ+ehDZ42xi1hgcweyo2NHQaAWYFqTT1qo6cNcWvIkadmfsuzBe8V0me7BjMkSq0htv5/1P4efVMJlNiim9LiXgRT0oIZj7n5ilpDDDkftb9H31QG6bX0CFobGpDllz7PNawRoJYWU/Bnbe/MqBSmLUi2aWH6cMowwF6AWlrknZy1vTOjctham9WGXxdqFX8b8Yz091/DBUJqKZ2N/Y9EswF4Yr5eqHWTDUqnp7/7GnymLwtKzSliaRdnp6LZGDVG0af2AkiQslqQmlP0mFNbOxDNxmCNQY2a0gsAYghKzSFyTK4+iKLZGD8q1KipvQB+568Fpe7Rvws3E9Oi2SC1amovwBRhda+IkaW29YhoNshc2YM8tdm7j+Ai03L3vu/UXgA4FFBTRYXu1KbOiGaDzJEsRLc77fNHdB6fev7kI5C0MxSqroytF3iKOwmrsfpL4VDsYwjRbJB7p8/IkiKWkN57SziRxgPUUDHuZyYptaVINBvks8JUfVEYfMfcAAI5PVVYVtNFrzO1oYtEs0Gm7rnPph508dN71gRpnOYHqGt6tM5/KNFskA8KY8XvtHDxH/l1QamkSRc/RLNBxu65P2XBTg0wrajUqSZf/BDNBhnzdJw6N18L3gTUUXdd/BDNBiH4cUuMoV8rpN9vDW8C6u6LH6LZILdq7DM9ctgJZUdwEzAw2KfeLqQ2MZpoNghJOpdEGeSzUkg7gZsaNzfVj2YdwkazQS6tCCQV97lC+p29wOezmtD+xbp+isakNjCZaDYIhUSfigw6Kgiln98bfP57kqFU3eIGP6XW5E2i2SinIruvxtTeJSGnYUo+hKpb7OO3WKZqNBuF7j7iSVhzau/SvFEwdbh9EdsZndo7lmg2CisCW0ntXRqGPg4J2hUr+kYt6plKNBuFOX4v/seQIOUsQVtiLf9qPdhoyq5glsBqw/WLreJXT1SLpuwOekbMHxsbqE+cE7aWn1o56i6iKbuF6cL3C2YQbi/OAd39TWeroim7hyIjBEzVNvq0sEqQ7xbRlG4gRZqcCbWOqNMf9+jbimhKd7CWgv3jHBosI7JSmdOvbpYqmtItDA3IJiTvXN0nbqYMs14upGNdBdGU7qGUOVWWmJpS40RC2juFJlLRoynyHXRZGbP+oWBC0WXxtGcI1VzNiWiKBMhOYxxrivH/RQ+JnlKzq06jKXIDGjyJRUxn9RwvGLT/Xs1EU2QEZLAxTGAfehax9KSqA3xDiKbIHRD8YqjAbAJrEPYcO1ikSMeaRFNkZsh6Y6ELacgMG6jTuAelzWSbIpoiK8DQgZWKDB/oMRBMY4s3gowkzjCdNqb3wM/yO8D27kA1Hd6TEtq8PzX1iNbPpU0W8MxJNEUqhIuNJ+5Tpqyd54Ywh9J7N0U0RTqAJbj3iJ5Get+miKZIJ1Axaer6B2Y80ns2RTRFOoLg5JSbADGG9H5NEU2RzmBZ9NjpShb6pPdqimiKdMgLhWNp+SFidiG9T1NEU6RTmJYkuDdE7xXSezRFNEU6hkxG8hBuiVmE9PtNEU2RzqF46q1SaYvv2rMG0RSRQ4LRJ4VLerWQfq8poikiByiIQjpxUvMrASGaIvKIlDpMwDD9bFNEU0TOeLtwKtYhpJ9rimiKSITVhMeswfTvzRFNEbkIqcPs55f+rTmiKSJX2cX4H6IpIn0QTRHpg2iKSB9EU0T6IJoi0gfRFJE+iKaI9EE0RaQPoikifRBNEemDaIpIH0RTRPogmiLSB9EUkT6Ipoj0QTRFpA+iKSJ9EE0R6YNoikgfRFNE+iCaItIH0RSRPoimiPRBNEWkD6IpIn0QTRHpg2iKSB9EU0T6IJoi0gfRFJE+iKaI9EE0RaQPoikifRBNEemDaIpIH0RTRPogmiLSB9EUkT6Ipoj0QTRFpA+iKSJ9EE0R6YNoikgPfPO9/wHCqt2vbswWIAAAAABJRU5ErkJggg=='
                    });
                    
                }
            }
        },
        
        initialize: function () {
            // This method gets called once converse.initialize has been called
            // and the plugin itself has been loaded.

            // Inside this method, you have access to the closured
            // _converse object as an attribute on "this".
            // E.g. this._converse
            var _converse = this._converse;

            loginForm.initialize(_converse);
            ownRoomForm.initialize(_converse);
            profileForm.initialize(_converse);
            userForm.initialize(_converse);
            buddiesForm.initialize(_converse);
            roomsForm.initialize(_converse);
            mamForm.initialize(_converse);
            vcardForm.initialize(_converse);
        }
    });
});

