(function (root, factory) {
    define('mam-form', ['jquery', 'converse'], factory);
})(this, function ($, converse) {
    
    var _ = converse.env._,
        Strophe = converse.env.Strophe;

    this.mamForm = {

        initialize(_converse) {
            this._converse = _converse;
            this._fetchMessagesButton.click(this._fetchMessages.bind(this));
            this.hide();

            _converse.api.listen.on('initialized',  this.onInitialized.bind(this));
            _converse.api.listen.on('logout',  this.onLogout.bind(this));
        },

        onInitialized() {
            this.show();
        },

        onLogout() {
            this.hide();
        },

        _fetchMessages() {
            var _converse = this._converse;

            var options = {
                'before': '', // Page backwards from the most recent message
                'with': this.buddyJid,
                'max': this.messageCount
            };
            console.log('Fetching messages:', 'buddy=', this.buddyJid, 'count=', this.messageCount);
            _converse.queryForArchivedMessages(
                options,
                this._showMessages.bind(this),
                this._showMessages.bind(this)
            );
        },

        _showMessages(messages) {
            var messageList = this._messageList;
            messageList.empty();

            if (!messages)  {
                console.log('Failed to fetch archived messages');
                messageList.append($(
                    '<li class="list-group-item">Failed to fetch archived messages</li>'
                ));
                return;
            }
            if (messages.length == 0) {
                messageList.append($(
                    '<li class="list-group-item">No messages were found</li>'
                ));
                return;
            }

            _.each(messages, function (message) {
                console.log(message);
                
                const forwarded = message.querySelector('forwarded'); 
                if (!_.isNull(forwarded)) {
                    message = forwarded.querySelector('message');
                }

                const type = message.getAttribute('type');
                const body = (type === 'error') 
                    ? _.propertyOf(message.querySelector('error text'))('textContent') 
                    : _.propertyOf(message.querySelector('body'))('textContent');

                messageList.append($(
                    '<li class="list-group-item">' + 
                        body +
                    '</li>'
                ));
            });
        },

        _getMessageAttributes() {
            delay = delay || message.querySelector('delay');
            const type = message.getAttribute('type'),
                    body = this.getMessageBody(message);

            const delayed = !_.isNull(delay),
                is_groupchat = type === 'groupchat',
                chat_state = message.getElementsByTagName(_converse.COMPOSING).length && _converse.COMPOSING ||
                    message.getElementsByTagName(_converse.PAUSED).length && _converse.PAUSED ||
                    message.getElementsByTagName(_converse.INACTIVE).length && _converse.INACTIVE ||
                    message.getElementsByTagName(_converse.ACTIVE).length && _converse.ACTIVE ||
                    message.getElementsByTagName(_converse.GONE).length && _converse.GONE;

            let from;
            if (is_groupchat) {
                from = Strophe.unescapeNode(Strophe.getResourceFromJid(message.getAttribute('from')));
            } else {
                from = Strophe.getBareJidFromJid(message.getAttribute('from'));
            }
            const time = delayed ? delay.getAttribute('stamp') : moment().format();
            let sender, fullname;
            if ((is_groupchat && from === this.get('nick')) || (!is_groupchat && from === _converse.bare_jid)) {
                sender = 'me';
                fullname = _converse.xmppstatus.get('fullname') || from;
            } else {
                sender = 'them';
                fullname = this.get('fullname') || from;
            }
            return {
                'type': type,
                'chat_state': chat_state,
                'delayed': delayed,
                'fullname': fullname,
                'message': body || undefined,
                'msgid': message.getAttribute('id'),
                'sender': sender,
                'time': time
            };
        },

        show() {
            this._container.show();
        },

        hide() {
            this._container.hide();
        },

        get _container() {
            return $('.mam-container');
        },

        get messageCount() {
            return this._messageCount.val();
        },

        set messageCount(count) {
            this._messageCount.val(count);
        },

        get _messageCount() {
            return $('.mam-count-input');
        },

        get buddyJid() {
            return this._buddyJid.val();
        },

        set buddyJid(jid) {
            this._buddyJid.val(jid);
        },

        get _buddyJid() {
            return $('.mam-jid-input');
        },

        get _fetchMessagesButton() {
            return $('.mam-fetch-btn');
        },

        get _messageList() {
            return $('.mam-message-list');
        }
    };

    return mamForm;
});