(function (root, factory) {
    define('own-room-form', ['jquery', 'converse'], factory);
})(this, function ($, converse) {

    var ownRoomForm = {

        initialize(_converse) {
            this._converse = _converse;
            this.openRoomButton.click(this._openRoom.bind(this));
        },

        _openRoom() {
            var _converse = this._converse;
            var roomJid = this.roomName + '@' + this.domainName;
            _converse.api.rooms.open(roomJid);
        },

        get roomName() {
            return $('.muc-own-room-name-input').val();
        },

        get domainName() {
            return $('.muc-own-room-domain-input').val();
        },

        get openRoomButton() {
            return $('.muc-open-own-room-btn');
        }
    };

    return ownRoomForm;

});