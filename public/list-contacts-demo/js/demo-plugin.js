(function (root, factory) {
    // AMD. Register as a module called "demo-plugin"
    define(
        'demo-plugin', 
        ['jquery', 'converse'], 
        factory
    );
    
})(this, function ($, converse) {

    // Commonly used utilities and variables can be found under the "env"
    // namespace of the "converse" global.
    var Strophe = converse.env.Strophe,
        $iq = converse.env.$iq,
        $msg = converse.env.$msg,
        $pres = converse.env.$pres,
        $build = converse.env.$build,
        b64_sha1 = converse.env.b64_sha1,
        _ = converse.env._,
        moment = converse.env.moment;

    converse.plugins.add('demo-plugin', {
        
        initialize: function () {
            // This method gets called once converse.initialize has been called
            // and the plugin itself has been loaded.

            // Inside this method, you have access to the closured
            // _converse object as an attribute on "this".
            // E.g. this._converse
            console.log('custom plugin has been loaded');

            this.findContacts = this.findContacts.bind(this);
            this.logIn = this.logIn.bind(this);
            this.logOut = this.logOut.bind(this);
            this.getLoginButton().click(this.logIn);
        },

        findContacts: function() {
            var _converse = this._converse;
            this.clearContactList();
            this.logIn(_converse);
        },

        logIn: function() {
            var _converse = this._converse;
            var username = this.getUserName();
            var password = this.getUserPassword();

            console.log('Logging in as ' + username + ' ' + password + '...');
            _converse.api.listen.once('rosterContactsFetched', this.showUserContacts.bind(this, _converse));
            _converse.api.listen.on('contactStatusChanged', this.changeContactStatus.bind(this, _converse));
            _converse.connection.connect(username, password, _converse.onConnectStatusChanged);
            console.log('Logged in as ' + username);

            var $loginBtn = this.getLoginButton();
            $loginBtn.off();
            $loginBtn.click(this.logOut);
            $loginBtn.val('Log Out');

            this.getUserInput().prop('disabled', true);
            this.getPasswordInput().prop('disabled', true);
        },

        showUserContacts: function(converse) {
            var _converse = converse;

            console.log('Loading contacts...');
            var contacts = _converse.api.contacts.get();
            console.log('Contacts:', contacts);

            this.buildContactList(contacts);
            this.updateProfiles(contacts);
        },

        updateProfiles: function(contacts) {
            var that = this;
            $.getJSON('/profiles', function(data) {
                var profiles = data.profiles;
                that.buildProfileList(profiles, contacts);
            });
        },

        buildContactList: function(contacts) {
            var $contactList = this.getContactList();
            $contactList.empty();

            var $contacts = _.map(contacts, function(contact) {
                return $(
                    '<li class="list-group-item">' + 
                        '<span>' + 
                            contact.jid +
                        '</span>' +
                        '<span class="badge">' + 
                            contact.chat_status +
                        '</span>' +
                    '</li>'
                );
            });

            _.forEach($contacts, function($contact) {
                $contactList.append($contact);
            });
        },

        buildProfileList: function(profiles, contacts) {
            var contactByJid = _.reduce(contacts, function(result, contact) {
                result[contact.jid] = contact;
                return result;
            }, {});

            var $profileList = this.getProfileList();
            $profileList.empty();

            var $profiles = _.map(profiles, function(profile) {
                var isFriend = _.has(contactByJid, profile.jid);
                var badge = isFriend ? contactByJid[profile.jid].chat_status : 'Add Buddy';
                return $(
                    '<li class="list-group-item">' + 
                        '<span>' + 
                            profile.jid +
                        '</span>' +
                        '<span class="badge">' + 
                            badge +
                        '</span>' +
                    '</li>'
                );
            });

            _.forEach($profiles, function($profile) {
                $profileList.append($profile);
            });
        },

        changeContactStatus: function(converse, attributes) {
            console.log('changeContactStatus:', attributes);
            this.showUserContacts(converse);
        },

        logOut: function() {
            var _converse = this._converse;
            var username = this.getUserName();
            var password = this.getUserPassword();

            console.log('Logging out: ' + username + '...');
            _converse.api.user.logout();
            _converse.connection.reset();
            console.log('Logged out: ' + username);

            var $contactList = this.getContactList();
            $contactList.empty();

            var $loginBtn = this.getLoginButton();
            $loginBtn.off();
            $loginBtn.click(this.logIn);
            $loginBtn.val('Log In');

            this.getUserInput().prop('disabled', false);
            this.getPasswordInput().prop('disabled', false);
        },

        getLoginButton: function() {
            return $('.login-btn');
        },

        getUserInput: function() {
            return $('.user-input');
        },

        getUserName: function() {
            return $('.user-input').val();
        },

        getPasswordInput: function() {
            return $('.password-input')
        },

        getUserPassword: function() {
            return $('.password-input').val();
        },

        getContactList: function() {
            return $('.contact-list');
        },

        getProfileList: function() {
            return $('.profile-list');
        }
    });
});

