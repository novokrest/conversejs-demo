require.config({
    baseUrl: '.',
    paths: {
        'jquery': 'https://code.jquery.com/jquery-3.2.1.min',
        'converse': 'https://cdn.conversejs.org/dist/converse.min',
        'demo': 'js/demo',
        'demo-plugin': 'js/demo-plugin',
        'loader': 'js/loader'
    }
});