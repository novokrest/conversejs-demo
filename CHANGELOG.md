# Changelog

## 0.0.12((2017-07-23))

### UI
- supports showing and changing current logged in user's status
- show buddy list with `Remove` button to remove given buddy from contacts

## 0.0.1 ((2017-07-22))

### UI
- `converse.js` messanger is appeared after user was logged in
- `Add Buddy` button is displayed for non-friend's profile
- clicking `Add buddy` button causes adding buddy in converse.js chat messenger 
- `Send Message` button is displayed for friend's profile
- clicking on `Send Message` button causes chat window for given friend is opened
